# Client
---
### _This is my first project written in c# programming language_
---
#### _Purpose_
```
The client is needed to send a request to the server in order to be
able to provide data or perform a certain group of system actions.
(the link to the repository will be attached below)
```
--- 
#### _Command_

[The list of available commands is presented in the server preview](https://gitlab.com/c6779/server/-/blob/dev/README.md)

---
#### _Work exemple_
![](https://i.imgur.com/8Pndq3F.jpg)
---
#### _Links_
- [Link to server repository](https://gitlab.com/c6779/server)
- [Link to the full description of the project](https://docs.google.com/document/d/1bsQ0ZEBnM-VsMyh965cZgceav1-NSgfky9RH6HX3aIY/edit?usp=sharing)
- [Help in writing a project](https://gitlab.com/dimmarvel)