﻿using Grpc.Core;
using System;
using System.Collections.Generic;
using System.Text;

namespace client.Src.JsonRequests
{
    public enum STATUS
    {
        progress,
        end
    }

    public class Poll
    {
        public string title;
        public STATUS status;
        public int votes;
        public List<Vote> votes_amount;
    }
}
