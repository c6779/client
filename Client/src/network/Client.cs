﻿using System;
using client.src.network.connection;
using NetworkApi;
using client.Src.Json;
using Newtonsoft.Json;
using client.src.Log;
using System.Threading;
using System.Collections.Generic;
using client.Src.Network;
using client.Src;

namespace client.src.network
{
    internal class Client
    {
        private Connection _connect;
        private Greeter.GreeterClient _client;
        private Command _command;
        private Account _account;
        private Timer _timer;

        public Client()
        {
            _connect = new Connection();
            _client = new Greeter.GreeterClient(_connect.channel);
            _command = new Command();
            _account= new Account();
            TimerCallback tm = new TimerCallback(Ping);
            _timer = new Timer(tm, null, 0, Utlis.SEND_PING_TIME_MS);
        }

        public Client(Connection connect)
        {
            _connect = connect;
            _client = new Greeter.GreeterClient(_connect.channel);
            _command = new Command();
        }

        public Reply SendMessage(string message)
        {
            HelloRequest name = new HelloRequest();
            name.Command = message;
            var reply = _client.Answer(name);

            return reply;
        }
        
        public void Ping(object id)
        {
            _command.command = "ping";
            _command.parametrs.Add(_account.id.ToString());
            var comm = JsonConvert.SerializeObject(_command);
            this.SendMessage(comm);
            _command.parametrs.Clear();
        }

        public void Query()
        {
            while (true)
            {
                string message  = Console.ReadLine();
                _command.Split(message);

                if (message == "end")
                {
                    break;
                }
                else if (_command.command == "loggin")
                {
                    Logger.GetLog().Info($"Input message: {message}");
                    _command.parametrs.Add(_account.id.ToString());
                    var comm = JsonConvert.SerializeObject(_command);
                    var answer = SendMessage(comm).Message;
                    if (answer == "Wrong login or password" || answer == Utlis.WRONG_PARAMETRS ||
                        answer == "Already logged in" || answer == Utlis.WRONG_ACCOUNT)
                    {
                        if (_account.id != 0)
                        {
                            Logger.GetLog().Info(answer);
                        }
                        else
                        {
                            _account.id = 0;
                            Logger.GetLog().Info(answer);
                        }
                    }
                    else
                    {
                        _account.id = Convert.ToInt32(answer);
                        Logger.GetLog().Info("Reply message: Authorization was successful");
                    }
                }
                else if (_command.command == "polls")
                {
                    Logger.GetLog().Info($"Input message: {message}");
                    var comm = JsonConvert.SerializeObject(_command);

                    Logger.GetLog().Info("Reply message: " + SendMessage(comm).Message);
                }
                else if (_command.command == "active_users")
                {
                    Logger.GetLog().Info($"Input message: {message}");
                    var comm = JsonConvert.SerializeObject(_command);

                    var active_users = JsonConvert.DeserializeObject<List<Account>>(SendMessage(comm).Message);
                    Logger.GetLog().Info("Active now: " );

                    foreach (var item in active_users)
                        Logger.GetLog().Info($"Id - {item.id} Loggin - {item.loggin} LastPing - {item.last_ping}");
                }
                else if (_command.command == "get_binance_data")
                {
                    Logger.GetLog().Info($"Input message: {message}");
                    _command.parametrs.Add(_account.id.ToString());
                    var comm = JsonConvert.SerializeObject(_command);

                    Logger.GetLog().Info("Reply message: " + SendMessage(comm));
                }
                else if (_command.command == "enter_room")
                {
                    Logger.GetLog().Info($"Input message: {message}");
                    _command.parametrs.Add(_account.id.ToString());
                    string comm = JsonConvert.SerializeObject(_command);
                    string answer = SendMessage(comm).Message;
                    if (answer == null)
                    {
                        Logger.GetLog().Info("You can enter the room within 30 seconds");
                    }
                    else if (answer == Utlis.NEED_LOGIN || answer == Utlis.WRONG_ROOM ||
                        answer == Utlis.WRONG_PLAYER || answer == Utlis.WRONG_ACCOUNT || answer == Utlis.WRONG_PARAMETRS)
                    {
                        Logger.GetLog().Info(answer);
                    }
                    else
                    {
                        List<string> gameHistory = JsonConvert.DeserializeObject<List<string>>(answer);
                        foreach (var item in gameHistory)
                        {
                            Logger.GetLog().Info(item); 
                            Thread.Sleep(200);
                        }
                    }
                }
                else
                {
                    Logger.GetLog().Info($"Input message: {message}");
                    _command.parametrs.Add(_account.id.ToString());
                    var comm = JsonConvert.SerializeObject(_command);
                    var answer = SendMessage(comm).Message;
                    Logger.GetLog().Info("Reply message: " + answer);
                }
                _command.parametrs.Clear();
            }
        }
    }
}
