﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace client.src.Log
{
    internal class Logger
    {
        private static NLog.Logger log;

        private Logger() { }

        public static NLog.Logger GetLog()
        {
            if (log == null)
                log = NLog.LogManager.GetCurrentClassLogger();
            return log;
        }
    }
}