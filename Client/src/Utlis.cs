﻿using System;
using System.Collections.Generic;
using System.Text;

namespace client.Src
{
    internal class Utlis
    {
        public const string NEED_LOGIN = "You need to be logged in to use this command";
        public const string WRONG_ROOM = "Room not found";
        public const string WRONG_PLAYER = "Player not found";
        public const string WRONG_ACCOUNT = "Account not found";
        public const string WRONG_PARAMETRS = "The wrong number of parameters";
        public const uint SEND_PING_TIME_MS = 5000;
    }
}
