﻿using System;
using client.src.Log;
using client.src.network;
using client.Src.Json;
using Newtonsoft.Json;
using client.Src.JsonRequests;
using Microsoft.VisualBasic;

namespace client
{

    internal class Program
    {
        static void Main(string[] args)
        {
            AppContext.SetSwitch("System.Net.Http.SocketsHttpHandler.Http2UnencryptedSupport", true);
            Client client = new Client();
            client.Query();
        }
    }
}
